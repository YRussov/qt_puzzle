#ifndef SETUPDIALOG_H
#define SETUPDIALOG_H

#include <QDialog>
#include <QSize>
#include <QGraphicsView>
#include "logic.h"
#include <QMessageBox>

namespace Ui {
class SetupDialog;
}

class SetupDialog : public QDialog
{
    Q_OBJECT

public:

    explicit SetupDialog(QWidget *parent = 0);

    QString m_imageFilePath;

    QSize m_puzzleSize=QSize(8,8);
    QSize puzzleSize();

    QPixmap pixmap();
    ~SetupDialog();

private slots:
    // активация кнопки обзор
    void on_pushButton_Browse_clicked();
    void on_pushButton_Ok_clicked();


private:
    Ui::SetupDialog *ui;
};

#endif // SETUPDIALOG_H
