#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QPalette>
#include <QFileDialog>
#include <QImageReader>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}
 
QPixmap MainWindow::pixmap(){
    return ui->frame->m_pixmap;
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_Browse_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"),"/home",tr("Images (*.png *.xpm *.jpg)"));
    if (!fileName.isEmpty()){
        ui->lineEdit->setText(fileName);
        ui->frame->setPicture(fileName);
    }
}

void MainWindow::on_pushButton_Ok_clicked()
{
    if(!pixmap().isNull()){
    QGraphicsView * view=new QGraphicsView;
    GameLogic gl(m_puzzleSize,ui->frame->pixmap());
    view->setScene(&gl);
    view->show();
    this->close();
    }
    else{
          QMessageBox::information(0,"Error","You did not select an image!");
    }
}
