#include "logic.h"
#include<QDebug>
Logic::Logic(QSize size, QPixmap img)
{

    setup(size, img);
}

void Logic::setup(QSize size, QPixmap pixmap)
{
    qsrand(QTime::currentTime().msec());
    clear();
    qDebug()<<size;

    Element::ConnectorPosition left;
    Element::ConnectorPosition buff[size.width()];

    for(int i=0;i<size.width();i++){
        buff[i]=Element::None;
    }

    for(int i = 0; i < size.height(); i++)
    {
        left = Element::None;

        for(int j = 0; j < size.width(); ++j)
        {
            Element::ConnectorPosition puzzle_arr[4];

            puzzle_arr[0] = buff[j];
            puzzle_arr[3] = left;

            for (int i = 1; i < 3; i++)
            {
                if (qrand() % 2)
                {
                    puzzle_arr[i] = Element::In;
                }
                else
                {
                    puzzle_arr[i] = Element::Out;
                }
            }

            buff[j]= reverse(puzzle_arr[2]);

            left =reverse(puzzle_arr[1]);

            if(j==size.width()-1)
                puzzle_arr[1] = Element::None;
            if(i==size.height()-1)
                puzzle_arr[2] = Element::None;

            Element *piece = new Element(puzzle_arr[0], puzzle_arr[1], puzzle_arr[2], puzzle_arr[3]);
            addItem(piece);

            QRect rect = piece->boundingRect().toRect();
            int cellSize = 50;
            rect.translate(cellSize/2+j*cellSize, cellSize/2+i*cellSize);
            QPixmap pix = pixmap.copy(rect);

            piece->setPixmap(pix);
            piece->setCoordinates(QPoint(i,j));
            piece->setSceneSize(size);
            piece->setPos(QPoint(qrand() % (size.width())*cellSize, qrand() % (size.height())*cellSize));
        }

    }
}
Element::ConnectorPosition Logic::reverse(Element::ConnectorPosition pos)
{
    switch(pos)
    {
        case Element::None: return Element::None;
        case Element::In: return Element::Out;
        case Element::Out: return Element::In;
    }
    return Element::None;
}



