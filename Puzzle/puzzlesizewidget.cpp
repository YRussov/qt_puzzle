#include "puzzlesizewidget.h"

PuzzleSizeWidget::PuzzleSizeWidget(QWidget *parent) : QFrame(parent)
{
    connect(this , SIGNAL(valueChanged(QSize)),this, SLOT(repaint()));
}

QSize PuzzleSizeWidget::value() const {
return size;
}

void PuzzleSizeWidget::setValue(const QSize &s) {
if(size == s
        || size.height()<min_size.height()
        || size.width()<min_size.width()
        || size.height()>max_size.height()
        || size.width()>max_size.width()
   )
    return;
size = s;
emit valueChanged(s);
// перерисовка виджета
update();
}

void PuzzleSizeWidget::setPicture(QString pix)
{
    if (m_pixmap.load(pix)) {
        // перерисовка виджета
       update();
    }
}

// Отрисовка виджета
void PuzzleSizeWidget::paintEvent(QPaintEvent * event){

QPainter painter(this);

// отрисовка подсвеченной области с количеством выбранных частей
renderValue(&painter);

// отрисовка сетки максимального размера
renderGrid(&painter);
}

// возвращает размер одного элемента, вычисленный на основе max_size и текущего размера виджета
QSize PuzzleSizeWidget::cellSize() const {
int w = width();
int h = height();
int mw = max_size.width();
int mh = max_size.height();
int extent = qMin(w/mw, h/mh);

// с проверкой что размер элемента не меньше чем 4x4 пикселя
return QSize(extent,extent).expandedTo(QApplication::globalStrut()).expandedTo(QSize(4,4));
}

// отрисовка элемента
void PuzzleSizeWidget::renderValue(QPainter *p){

    QSize size(cellSize().width()*value().width(), cellSize().height()*value().height());

    if(!m_pixmap.isNull())
    {
        p->drawPixmap(0,0,m_pixmap.scaled(size));
    }
    else{
        p->setBrush(Qt::darkRed);
        p->drawRect(0,0,size.width(), size.height());
    }
}


void PuzzleSizeWidget::renderGrid(QPainter *p){

    //сброс кисти
    p->setBrush(Qt::NoBrush);

    // пробегаем элементы по горизонтали и вертикали с отображением границы через drawRect
    for(int i=0;i<max_size.width();i++){
        for(int j=0;j<max_size.height();j++){
            p->drawRect(i*cellSize().width(), j*cellSize().height(), cellSize().width(), cellSize().height());
        }
    }
}

// проверка выхода за границы
bool PuzzleSizeWidget::checkBorder(QMouseEvent *e)
{
    if(e->pos().x() < max_size.width()*cellSize().width()
            && e->pos().y() < max_size.height()*cellSize().height()
            && e->pos().x() > min_size.width()*cellSize().width()
            && e->pos().y() > min_size.height()*cellSize().height())
    {
        return true;
    }
    else
    {
        return false;
    }
}

// обработка сообщений мыши
void PuzzleSizeWidget::mousePressEvent(QMouseEvent *e){
       if(e->button() == Qt::LeftButton)
       {
           // проверка выхода за границы
           if(checkBorder(e))
           {
               int x = e->pos().x()/cellSize().width();
               int y = e->pos().y()/cellSize().height();
               setValue(QSize(x+1,y+1));
           }
       }
       else
           return;
}

// обработка сообщений мыши
void PuzzleSizeWidget::mouseMoveEvent(QMouseEvent *e){
    // pos() - получение координат мыши
    // проверка выхода за границы
    if(checkBorder(e))
    {
        int x = e->pos().x()/cellSize().width();
        int y = e->pos().y()/cellSize().height();
        setValue(QSize(x+1,y+1));
    }
    else
        return;
}

// установка и получение изображения для элемента. предпросмотр пазла при изменении настроек
QPixmap PuzzleSizeWidget::pixmap()
{
    QSize sz(50*size.width(), 50*size.height());
    return m_pixmap.scaled(sz);
}
