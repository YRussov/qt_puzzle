#ifndef LOGIC_H
#define LOGIC_H

#include <QObject>
#include <QWidget>
#include "element.h"
#include<QTime>
#include <QGraphicsScene>
#include <QPixmap>

class Logic : public QGraphicsScene
{
public:
    Logic(QSize size, QPixmap img);
    void setup(QSize size, QPixmap pixmap);
    Element::ConnectorPosition reverse(Element::ConnectorPosition pos);
};

#endif // LOGIC_H
