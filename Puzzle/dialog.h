#ifndef DIALOG_H
#define DIALOG_H
 
#include <QDialog>
#include <QSize>
#include <QGraphicsView>
#include "gamelogic.h"
#include <QMessageBox>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    QString m_imageFilePath;
    QSize m_puzzleSize=QSize(5,5);
    QPixmap pixmap();
    ~Dialog();

private slots:
    void on_pushButton_Browse_clicked();
    void on_pushButton_Ok_clicked();


private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
