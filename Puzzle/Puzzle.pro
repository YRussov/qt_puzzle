#-------------------------------------------------
#
# Project created by QtCreator 2017-11-09T18:47:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Puzzle
TEMPLATE = app


SOURCES += main.cpp\
    setupdialog.cpp \
    logic.cpp \
    element.cpp \
    puzzlesizewidget.cpp

HEADERS  += \
    setupdialog.h \
    logic.h \
    element.h \
    puzzlesizewidget.h

FORMS    += \
    setupdialog.ui

RESOURCES += \
    res.qrc

