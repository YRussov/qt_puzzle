#ifndef ELEMENT_H
#define ELEMENT_H

#include <QObject>
#include <QWidget>
#include <QGraphicsPathItem>
#include <QSize>
#include <QPen>
#include <QVector>
#include <QPainter>
#include <QMessageBox>
#include <QGraphicsScene>
#include <QApplication>

class Element : public QGraphicsPathItem
{
    qreal size=50;
    QPixmap m_pix;
    QSize sceneSize;
    QPoint m_coord;
    QVector <Element *> neighbors = QVector<Element *>(4,0);

public:

    enum ConnectorPosition { None, Out, In };
    enum Direction{ North, South, East,  West};
    ConnectorPosition c_position[4];

    Element(QWidget *parent = 0);
    Element(ConnectorPosition north, ConnectorPosition south, ConnectorPosition east, ConnectorPosition west);

    void constructShape(QPainterPath &painter);
    void singleBorder(QPainterPath &painter, ConnectorPosition var_position, double border_coeff[]);
    void setPixmap(QPixmap pix);
    void setCoordinates(QPoint coord);
    void setSceneSize(QSize sz);
    void link(Element* neighbor, Direction dir);
    void isNeighbors(QSet<QPoint> &checked);
    void neighbor_direction_f(Direction direction, float sub_direction_koefs[], Element *neib);
    void neighbor_search(Direction direction);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);

    QPixmap pixmap();
    QPoint getCoordinates();
    QSize getSceneSize();
    QVariant itemChange(QGraphicsPathItem::GraphicsItemChange change, const QVariant &value);

    Element::Direction reverse(Element::Direction dir);
};

#endif // ELEMENT_H
