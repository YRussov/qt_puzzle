#include "element.h"
#include <QDebug>

Element::Element(ConnectorPosition north, ConnectorPosition east, ConnectorPosition south, ConnectorPosition west)
{
    c_position[0]=north;
    c_position[1]=east;
    c_position[2]=south;
    c_position[3]=west;

    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges);

    QPainterPath painter;

    constructShape(painter);

    setPath(painter);
}

void Element::singleBorder(QPainterPath &painter, ConnectorPosition var_position, double border_coeff[])
{
    if(var_position==None){
        painter.lineTo(border_coeff[0]*size, border_coeff[1]*size);
    }
    else if(var_position==In){
        painter.lineTo(border_coeff[2]*size, border_coeff[3]*size);
        painter.lineTo(border_coeff[4]*size, border_coeff[5]*size);
        painter.lineTo(border_coeff[6]*size, border_coeff[7]*size);
        painter.lineTo(border_coeff[8]*size, border_coeff[9]*size);
    }
    else if(var_position==Out){
        painter.lineTo(border_coeff[2]*size, border_coeff[3]*size);
        painter.lineTo(border_coeff[10]*size, border_coeff[11]*size);
        painter.lineTo(border_coeff[6]*size, border_coeff[7]*size);
        painter.lineTo(border_coeff[8]*size, border_coeff[9]*size);
    }
}

void Element::constructShape(QPainterPath &painter)
{
    double border_coeff[4][12] = {{0.5, -0.5, -0.3, -0.5, 0, -0.3, 0.3, -0.5, 0.5, -0.5, 0, -0.7},
                                  {0.5, 0.5, 0.5, -0.3, 0.3, 0, 0.5, 0.3, 0.5, 0.5, 0.7, 0},
                                  {-0.5, 0.5, 0.3, 0.5, 0, 0.3, -0.3, 0.5, -0.5, 0.5, 0, 0.7},
                                  {-0.5, -0.5, -0.5, 0.3, -0.3, 0, -0.5, -0.3, -0.5, -0.5, -0.7, 0}};

    painter.moveTo(-1*size/2,-1*size/2);

    for (int i = 0; i < 4; i ++)
    {
        singleBorder(painter, c_position[i], border_coeff[i]);
    }
    painter.closeSubpath();
}



QPixmap Element::pixmap()
{
    return m_pix;
}

void Element::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    QSet<QPoint> checked;
    isNeighbors(checked);
    int p_num = sceneSize.width()*sceneSize.height();
    if(checked.count() == p_num)
    {
        QMessageBox::information(0, "Game Over!","Good luck next time", QMessageBox::Close);

    }

    QGraphicsPathItem::mouseReleaseEvent(event);
}

QVariant Element::itemChange(QGraphicsPathItem::GraphicsItemChange change, const QVariant &value)
{
    if (change==ItemPositionHasChanged)
       {
           QPoint new_pos=value.toPoint();
           if (neighbors[South]) neighbors[South]->setPos(new_pos.x(),new_pos.y()+50);
           if (neighbors[North]) neighbors[North]->setPos(new_pos.x(),new_pos.y()-50);
           if (neighbors[West]) neighbors[West]->setPos(new_pos.x()-50,new_pos.y());
           if (neighbors[East]) neighbors[East]->setPos(new_pos.x()+50,new_pos.y());
       }
    return QGraphicsPathItem::itemChange(change, value);
}

void Element::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setClipPath(path());
    QRect rect =boundingRect().toRect();

    painter->setPen(QPen(Qt::red, 4));
    painter->drawPixmap(rect, m_pix.scaled(rect.width(),rect.height()));
    painter->drawPath(path());
}
void Element::setPixmap(QPixmap pix)
{
    m_pix=pix;

    update();
}
void Element::setCoordinates(QPoint coord)
{
    m_coord = coord;
}

QPoint Element::getCoordinates()
{
    return m_coord;
}

 void Element::link(Element *neighbor, Element::Direction dir)
{
    neighbors[dir]=neighbor;

}

QSize Element::getSceneSize()
{
    return sceneSize;
}

void Element::setSceneSize(QSize sz)
{
    sceneSize=sz;
}

inline uint qHash (const QPoint & key)
{
    return qHash (QPair<int,int>(key.x(), key.y()) );
}

void Element::isNeighbors(QSet<QPoint> &checked)
{
    if(checked.contains(m_coord))  return;
    checked.insert(m_coord);
    neighbor_search(North);
    neighbor_search(East);
    neighbor_search(South);
    neighbor_search(West);

    if(neighbors[East])
    neighbors[East]->isNeighbors(checked);

    if(neighbors[West])
    neighbors[West]->isNeighbors(checked);

    if(neighbors[South])
    neighbors[South]->isNeighbors(checked);

    if(neighbors[North])
    neighbors[North]->isNeighbors(checked);
}

Element::Direction Element::reverse(Element::Direction dir)
{
    switch(dir)
    {
        case Element::North: return Element::South;
        case Element::South: return Element::North;
        case Element::East: return Element::West;
        case Element::West: return Element::East;
    }
    return Element::North;
}

void Element::neighbor_direction_f(Element::Direction direction, float sub_direction_koefs[], Element *neib)
{
    //Element* neib;
    neib = (Element*)(scene()->itemAt(pos().x()+sub_direction_koefs[0],pos().y()+sub_direction_koefs[1],QTransform()));
    if(!neib) return;
    if((neib->getCoordinates().x() == getCoordinates().x()+sub_direction_koefs[2]) && (neib->getCoordinates().y() == getCoordinates().y()+sub_direction_koefs[3]))
    {
        link(neib, direction);
        neib->link(this, reverse(direction));
        neib->setPos(pos().x()+sub_direction_koefs[0],pos().y()+sub_direction_koefs[1]);
        return;
    }
}

void Element::neighbor_search(Element::Direction direction)
{
    if (neighbors[direction])
        return;



    float direction_koefs[4][4] = {{0, 50, 1, 0},
                         {0, -50, -1, 0},
                         {50, 0, 0 , 1},
                         {-50, 0, 0 , -1}};

    Element* neib;

    switch(direction)
    {
        case Element::South:
        {
            neighbor_direction_f(direction, direction_koefs[0], neib);
            break;
        }
        case Element::North:
        {
            neighbor_direction_f(direction, direction_koefs[1], neib);
            break;
        }
        case Element::East:
        {
            neighbor_direction_f(direction, direction_koefs[2], neib);
            break;
        }
        case Element::West:
        {
            neighbor_direction_f(direction, direction_koefs[3], neib);
            break;
        }
    }

}
