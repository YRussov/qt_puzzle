#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSize>
#include <QGraphicsView>
#include "gamelogic.h"
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public: 
    explicit MainWindow(QWidget *parent = 0);
    QString m_imageFilePath;
    QSize m_puzzleSize=QSize(5,5);
    QPixmap pixmap();
    ~MainWindow();

private slots:
    void on_pushButton_Browse_clicked();

    void on_pushButton_Ok_clicked();

private:
    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
