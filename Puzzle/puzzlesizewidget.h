#ifndef WidgetSize_H
#define WidgetSize_H

#include <QWidget>
#include <QFrame>
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
#include <QApplication>

class PuzzleSizeWidget : public QFrame
{
    Q_OBJECT

    // требование : Минимально возможный размер поля 2х2, максимальный 8х8
    QSize min_size=QSize(2,2);
    QSize max_size=QSize(8,8);

public:

    // наследуется от QFrame. Заменяет QFrame в диалоге настройки
    PuzzleSizeWidget(QWidget *parent = 0);

    void paintEvent(QPaintEvent *event);
    bool checkBorder(QMouseEvent * e);
    void mousePressEvent(QMouseEvent * e);
    void mouseMoveEvent(QMouseEvent * e);
    void setValue(const QSize &s);
    void setPicture(QString pix);
    void renderValue(QPainter *p);
    void renderGrid(QPainter *p);

    // размер поля по умолчанию
    QSize size=QSize(6,7);
    QSize cellSize() const;
    QSize value() const;
    QPixmap pixmap();
    QPixmap m_pixmap;

    Q_PROPERTY(QSize value READ value WRITE setValue)

signals:
    void valueChanged(const QSize&);
    void horizontalValueChanged(int);
    void verticalValueChanged(int);
};

#endif // WidgetSize_H
