#include "dialog.h"
#include "ui_Dialog.h"
#include <QDebug>
#include <QPalette>
#include <QFileDialog>
#include <QImageReader>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

}
 
QPixmap Dialog::pixmap(){
    return ui->frame->m_pixmap;
}

Dialog::~Dialog()
{
    delete ui;
}


void Dialog::on_pushButton_Browse_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"),"/home",tr("Images (*.png *.xpm *.jpg)"));
    if (!fileName.isEmpty()){
        ui->lineEdit->setText(fileName);
        ui->frame->setPicture(fileName);
    }
}

void Dialog::on_pushButton_Ok_clicked()
{
    if(!pixmap().isNull()){
    QGraphicsView * view=new QGraphicsView;
    GameLogic gl(m_puzzleSize,ui->frame->pixmap());
    view->setScene(&gl);
    view->show();
    this->close();
    }
    else{
          QMessageBox::information(0,"Error","You did not select an image!");
    }
}
